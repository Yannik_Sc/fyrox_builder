use fyrox::core::algebra::{Matrix4, Vector2, Vector3};
use fyrox::core::color::Color;
use fyrox::core::inspect::{Inspect, PropertyInfo};
use fyrox::core::math::Matrix4Ext;
use fyrox::core::reflect::Reflect;
use fyrox::core::visitor::{Visit, VisitResult, Visitor};
use fyrox::event::{ElementState, MouseButton, WindowEvent};
use fyrox::impl_component_provider;

use crate::{uuid, Event, Game, GameConstructor, Uuid};
use fyrox::scene::node::TypeUuidProvider;
use fyrox::script::{ScriptContext, ScriptTrait};

#[derive(Clone, Debug, Default, Inspect, Reflect, Visit)]
pub struct PathNode {
    resistance: f32,
    position: Vector2<f32>,
    highlight: bool,
}

#[derive(Clone, Debug, Default, Inspect, Reflect, Visit)]
pub struct AStarScript {
    size: Vector2<f32>,
    node_radius: f32,

    #[reflect(hidden)]
    nodes: Vec<PathNode>,

    #[reflect(hidden)]
    mouse_pos: Vector2<f32>,

    #[reflect(hidden)]
    clicked: Option<Vector2<f32>>,
}

impl_component_provider!(AStarScript);

impl TypeUuidProvider for AStarScript {
    fn type_uuid() -> Uuid {
        uuid!("a285569a-19b2-4cfc-8438-6d2dfb881db3")
    }
}

impl AStarScript {
    pub fn init_grid(&mut self) {
        let mut node_x = 0.0;
        while node_x < self.size.x {
            node_x += self.node_radius * 2.0;

            let mut node_y = 0.0;
            while node_y < self.size.y {
                node_y += self.node_radius * 2.0;

                self.nodes.push(PathNode {
                    resistance: 0.0,
                    position: Vector2::new(node_y as f32, node_x as f32),
                    ..Default::default()
                });
            }
        }

        println!("Initialized grid with {} nodes", self.nodes.len());
    }

    pub fn add_click(&mut self, coords: Vector2<f32>) {
        let percent = Vector2::new(
            coords.x / (self.size.x - self.node_radius),
            coords.y / (self.size.y - self.node_radius),
        );

        let max_x = self.size.x / (self.node_radius * 2.0);
        let max_y = self.size.y / (self.node_radius * 2.0);
        let x = ((percent.x * max_x).round() + max_x / 2.0) as i32;
        let y = ((percent.y * max_y).round() + max_y / 2.0) as i32;
        let x = x.clamp(0, max_x as i32 - 1);
        let y = y.clamp(0, max_y as i32 - 1);

        if self.clicked.is_none() {
            self.clicked = Some(Vector2::new(x as f32, y as f32));

            return;
        }

        let pos1 = self.clicked.take().unwrap();
        let pos2 = Vector2::new(x as f32, y as f32);

        println!("Optimal cost: {}", self.optimal_cost(pos1, pos2));
    }

    pub fn optimal_cost(&self, from: Vector2<f32>, to: Vector2<f32>) -> i32 {
        let move_x = (from.x - to.x).abs();
        let move_y = (from.y - to.y).abs();
        let diag = move_x.min(move_y) as i32;
        let stre = (move_x - move_y).abs() as i32;

        println!("moves: diag, {} ; stre, {}", diag, stre);
        println!("Diff: {},{}", from.x - to.x, from.y - to.y);

        println!("Generating path {:?} -> {:?}", from, to);

        diag * 14 + stre * 10
    }

    pub fn node_pos_to_index(&self, x: i32, y: i32) -> usize {
        let max_y = self.size.y / (self.node_radius * 2.0);

        (x + max_y.round() as i32 * y) as usize
    }
}

impl ScriptTrait for AStarScript {
    fn on_init(&mut self, _context: ScriptContext) {
        self.init_grid();
    }

    fn on_os_event(&mut self, event: &Event<()>, context: ScriptContext) {
        let plugin = if let Some(plugin) = context.plugin.cast::<Game>() {
            plugin
        } else {
            return;
        };

        if let Event::WindowEvent { event, .. } = event {
            if let WindowEvent::CursorMoved { position, .. } = event {
                let logical = position.to_logical::<f32>(plugin.scale_factor);
                let position = Vector3::new(logical.x, logical.y, 0.0);
                let size = plugin.window_inner_size.to_logical(plugin.scale_factor);
                let size = Vector3::new(size.width, size.height, 0.0);
                let position = (position - (size / 2.0)) / -200.0;

                self.mouse_pos = position.xy();

                context.scene.drawing_context.clear_lines();
                context.scene.drawing_context.draw_rectangle(
                    0.1,
                    0.1,
                    Matrix4::new_translation(&position),
                    Color::BLUE,
                );
            }

            if let WindowEvent::MouseInput {
                button: MouseButton::Left,
                state: ElementState::Pressed,
                ..
            } = event
            {
                self.add_click(self.mouse_pos);
            }
        }
    }

    fn on_update(&mut self, context: ScriptContext) {
        let ScriptContext { scene, handle, .. } = context;

        let node = &scene.graph[handle];
        let transform = node.global_transform();

        let top_left = transform.position().xy() - (self.size / 2.0);
        scene.drawing_context.draw_rectangle(
            0.1,
            0.1,
            Matrix4::new_translation(&Vector3::new(top_left.x, top_left.y, 0.0)),
            Color::BLUE,
        );

        for node in &self.nodes {
            let node_pos =
                top_left + node.position - Vector2::new(self.node_radius, self.node_radius);
            let translation = Matrix4::new_translation(&Vector3::new(node_pos.x, node_pos.y, 0.0));
            let color = if node.highlight {
                Color::ORANGE
            } else {
                Color::WHITE
            };

            scene
                .drawing_context
                .draw_rectangle(0.01, 0.01, translation, color);
            scene.drawing_context.draw_rectangle(
                self.node_radius - 0.01,
                self.node_radius - 0.01,
                translation,
                color,
            );
        }

        scene.drawing_context.draw_rectangle(
            self.size.x / 2.0,
            self.size.y / 2.0,
            transform,
            Color::WHITE,
        );
    }

    fn id(&self) -> Uuid {
        Self::type_uuid()
    }

    fn plugin_uuid(&self) -> Uuid {
        GameConstructor::type_uuid()
    }
}
