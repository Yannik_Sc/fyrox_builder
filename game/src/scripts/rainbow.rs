use fyrox::core::algebra::Matrix4;
use fyrox::core::color::{Color, Hsv};
use fyrox::core::inspect::{Inspect, PropertyInfo};
use fyrox::core::reflect::Reflect;
use fyrox::core::visitor::{Visit, VisitResult, Visitor};
use fyrox::impl_component_provider;
use fyrox::scene::dim2::rectangle::Rectangle;

use crate::{uuid, GameConstructor, Uuid};
use fyrox::scene::node::TypeUuidProvider;
use fyrox::script::{ScriptContext, ScriptTrait};

#[derive(Clone, Debug, Default, Inspect, Reflect, Visit)]
pub struct RainbowScript(u8);

impl_component_provider!(RainbowScript);

impl TypeUuidProvider for RainbowScript {
    fn type_uuid() -> Uuid {
        uuid!("7bfaf5ea-c2c6-449b-8846-1b79f98b727c")
    }
}

impl ScriptTrait for RainbowScript {
    fn on_update(&mut self, context: ScriptContext) {
        let ScriptContext { scene, handle, .. } = context;
        scene
            .drawing_context
            .draw_rectangle(1.0, 1.0, Matrix4::new_scaling(1.0), Color::WHITE);
        let node = &mut scene.graph[handle];
        let rectangle = node.query_component_mut::<Rectangle>();

        if let Some(rectangle) = rectangle {
            let color = Color::from(Hsv::new((self.0 as f32 / 255.0) * 360.0, 100.0, 100.0));
            rectangle.set_color(color);

            self.0 %= 255;
            self.0 += 1;
        }
    }

    fn id(&self) -> Uuid {
        Self::type_uuid()
    }

    fn plugin_uuid(&self) -> Uuid {
        GameConstructor::type_uuid()
    }
}
